package com.example.mangtas_exam

import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*

object DateCalculator {

    const val DATE_FORMAT = "MM/dd/yyyy"

    var fixedDays = ArrayList<Calendar>()
    var weekendHoliday = ArrayList<Calendar>()
    var dynamicDays = ArrayList<Calendar>()
    var minusDynamicDays = 0
    var minusFixedDays = 0
    var fallinEndDate = 0

    //Result
    var fixDateCount = 0;
    var weekendHolidayCount = 0;
    var dynamicHolidayCount = 0;

    @RequiresApi(Build.VERSION_CODES.O)
    fun calculateWorkingDays(startDate: Long, endDate: Long): Int {
        for (holiday in DummyData.getListHoliday()) {
            when (holiday.type) {
                HolidayType.FIXED ->
                    fixDateCount += calculateFixedDate(
                        startDate,
                        endDate,
                        fixedDateMonth = holiday.month,
                        fixedDateDay = holiday.day
                    )
                HolidayType.WEEKEND -> weekendHolidayCount += calculateWeekendHoliday(
                    startDate,
                    endDate,
                    weekendHolidayMonth = holiday.month,
                    weekendHolidayDay = holiday.day
                )
                HolidayType.DYNAMIC -> dynamicHolidayCount += calculateDynamicDates(
                    startDate,
                    endDate,
                    dayOfWeek = holiday.dynamic.dayOfWeek,
                    dayOfWeekInMonth = holiday.dynamic.dayOfWeekInMonth,
                    month = holiday.dynamic.month
                )
            }
        }

        return (calculateDaysWithoutWeekend(
            startDate,
            endDate
        ) - fixDateCount + minusFixedDays - weekendHolidayCount - dynamicHolidayCount -
                -minusDynamicDays + fallinEndDate).coerceAtLeast(0)
    }

    // calculate days without weekend
    fun calculateDaysWithoutWeekend(startDate: Long, endDate: Long): Int {
        if (startDate == endDate) return 0
        val distance: Int = ((endDate - startDate) / (86400 * 1000)).toInt() - 1
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()
        start.timeInMillis = startDate
        end.timeInMillis = endDate
        var count = 0;
        while (start.before(end)) {
            val day = start.get(Calendar.DAY_OF_WEEK)
            if (day == Calendar.SATURDAY) {
                break
            }
            count++;
            start.add(Calendar.DATE, 1)
        }
        if (count > distance) return distance
        var times = (distance - count) / 7 + 1
        var weekdays = if ((distance - count) % 7 == 0) {
            distance - 2 * times + 1
        } else distance - 2 * times
        if (count == 0) { // start Saturday
            weekdays += 1
        }
        return weekdays
    }

    // Fixed date def: 11.16 each year
    // Calculate all fixed date
    fun calculateFixedDate(
        startDate: Long,
        endDate: Long,
        fixedDateMonth: Int = Calendar.NOVEMBER,
        fixedDateDay: Int = 16
    ): Int {
        fixedDays.clear()
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()
        start.timeInMillis = startDate
        end.timeInMillis = endDate
        val startYear = start.get(Calendar.YEAR)
        val endYear = end.get(Calendar.YEAR)
        for (year in startYear..endYear) {
            val calendar = Calendar.getInstance()
            calendar.set(year, fixedDateMonth, fixedDateDay)
            if (calendar.get(Calendar.DAY_OF_YEAR) != start.get(Calendar.DAY_OF_YEAR) && calendar.get(Calendar.DAY_OF_YEAR) != end.get(Calendar.DAY_OF_YEAR)) {
                fixedDays.add(calendar)
            }
        }

        val iterator = fixedDays.iterator()
        while (iterator.hasNext()) {
            val item = iterator.next()
            if (item < start || item > end) {
                iterator.remove()
            } else {
                val day = item.get(Calendar.DAY_OF_WEEK)
                if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
                    minusFixedDays++
                }
            }
        }
        return fixedDays.size
    }

    // Weekend holidays def: 11.13 each year
    // Calculate all weekend holiday
    @RequiresApi(Build.VERSION_CODES.O)
    fun calculateWeekendHoliday(
        startDate: Long,
        endDate: Long,
        weekendHolidayMonth: Int = Calendar.NOVEMBER,
        weekendHolidayDay: Int = 13
    ): Int {
        weekendHoliday.clear()
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()
        start.timeInMillis = startDate
        end.timeInMillis = endDate
        val startYear = start.get(Calendar.YEAR)
        val endYear = end.get(Calendar.YEAR)
        this.fallinEndDate = 0
        //var compensatoryDay = 0
        for (year in startYear..endYear) {
            val calendar: Calendar = Calendar.getInstance()
            calendar.set(year, weekendHolidayMonth, weekendHolidayDay)
            if (calendar.get(Calendar.DAY_OF_YEAR) != start.get(Calendar.DAY_OF_YEAR) && calendar.get(Calendar.DAY_OF_YEAR) != end.get(Calendar.DAY_OF_YEAR)) {
                weekendHoliday.add(calendar)
            }
        }

        val iterator = weekendHoliday.iterator()
        while (iterator.hasNext()) {
            val item = iterator.next()
            if (item < start || item > end) {
                iterator.remove()
            } else {
                val day = item.get(Calendar.DAY_OF_WEEK)
                if (day == Calendar.SATURDAY) {
                    item.add(Calendar.DATE, 2)
                    if (item >= end) fallinEndDate = 1
                }
                if (day == Calendar.SUNDAY) {
                    item.add(Calendar.DATE, 1)
                    if (item >= end) fallinEndDate = 1
                }
            }
        }

        return weekendHoliday.size
    }

    // Dynamic date def: each third Monday in November (each year)
    // Calculate all dynamic date
    fun calculateDynamicDates(
        startDate: Long,
        endDate: Long,
        dayOfWeek: Int = Calendar.MONDAY,
        dayOfWeekInMonth: Int = 3,
        month: Int = Calendar.NOVEMBER
    ): Int {
        dynamicDays.clear()
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()
        start.timeInMillis = startDate
        end.timeInMillis = endDate
        val startYear = start.get(Calendar.YEAR)
        val endYear = end.get(Calendar.YEAR)
        for (year in startYear..endYear) {
            val calendar = Calendar.getInstance()
            calendar[Calendar.DAY_OF_WEEK] = dayOfWeek
            calendar[Calendar.DAY_OF_WEEK_IN_MONTH] = dayOfWeekInMonth
            calendar[Calendar.MONTH] = month
            calendar[Calendar.YEAR] = year
            if (calendar.get(Calendar.DAY_OF_YEAR) != start.get(Calendar.DAY_OF_YEAR) && calendar.get(Calendar.DAY_OF_YEAR) != end.get(Calendar.DAY_OF_YEAR)) {
                dynamicDays.add(calendar)
            }
        }

        val iterator = dynamicDays.iterator()
        while (iterator.hasNext()) {
            val item = iterator.next()
            if (item < start || item > end) {
                iterator.remove()
            } else {
                val day = item.get(Calendar.DAY_OF_WEEK)
                if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
                    minusDynamicDays++
                }
            }
        }
        return dynamicDays.size
    }

    fun clearAll() {
        fixedDays.clear()
        weekendHoliday.clear()
        dynamicDays.clear()
        fixDateCount = 0
        dynamicHolidayCount = 0
        weekendHolidayCount = 0
        minusFixedDays = 0
        minusDynamicDays = 0
    }

}