package com.example.mangtas_exam

import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CalculateUnitTest {

    @Test
    fun dynamicDaysOutsideDates_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.OCTOBER, 30)
        endDate.set(2021, Calendar.DECEMBER, 30)
        assertEquals(
            0,
            DateCalculator.calculateDynamicDates(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.MONDAY,
                1,
                Calendar.JANUARY
            )
        )
    }

    // Dynamic date def: each third Monday in November (each year)
    @Test
    fun dynamicDaysInsideSameYear_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.OCTOBER, 30)
        endDate.set(2021, Calendar.DECEMBER, 30)
        assertEquals(
            1,
            DateCalculator.calculateDynamicDates(
                startDate.timeInMillis,
                endDate.timeInMillis
            )
        )
    }

    @Test
    fun dynamicDayInsideManyYears_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2018, Calendar.OCTOBER, 30)
        endDate.set(2022, Calendar.DECEMBER, 30)
        assertEquals(
            5,
            DateCalculator.calculateDynamicDates(
                startDate.timeInMillis,
                endDate.timeInMillis
            )
        )
    }

    @Test
    fun fixedHolidayOutsideDates_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.DECEMBER, 1)
        endDate.set(2021, Calendar.DECEMBER, 30)
        assertEquals(
            0,
            DateCalculator.calculateFixedDate(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.NOVEMBER,
                15
            )
        )
    }

    @Test
    fun fixedHolidayInsideAndFallinWorkingDay_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.NOVEMBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            1,
            DateCalculator.calculateFixedDate(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.NOVEMBER,
                15
            )
        )
    }

    @Test
    fun fixedHolidayInsideAndFallinWeekend_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.NOVEMBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            DateCalculator.calculateFixedDate(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.NOVEMBER,
                13
            ), 1
        )
    }

    @Test
    fun weekendHolidayOutsideDates_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.DECEMBER, 1)
        endDate.set(2021, Calendar.DECEMBER, 30)
        assertEquals(
            0,
            DateCalculator.calculateWeekendHoliday(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.NOVEMBER,
                10
            )
        )
    }


    @Test
    fun weekendHolidayInsideAndFallinWorkingDay_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.OCTOBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            1,
            DateCalculator.calculateWeekendHoliday(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.NOVEMBER,
                15
            )
        )
    }

    @Test
    fun weekendHolidayInsideAndFallinWeekend_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.NOVEMBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            1,
            DateCalculator.calculateWeekendHoliday(
                startDate.timeInMillis,
                endDate.timeInMillis,
                Calendar.NOVEMBER,
                13
            )
        )
    }

    //test doesn't cover all dummy date
    @Test
    fun calculateWorkingDays1_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.NOVEMBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            16,
            DateCalculator.calculateWorkingDays(
                startDate.timeInMillis,
                endDate.timeInMillis,
            )
        )
    }

    //test cover all dummy date
    @Test
    fun calculateWorkingDays2_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2021, Calendar.OCTOBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            11,
            DateCalculator.calculateWorkingDays(
                startDate.timeInMillis,
                endDate.timeInMillis,
            )
        )
    }

    //test cover all dummy date between two years
    @Test
    fun calculateWorkingDays3_isCorrect() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.set(2020, Calendar.OCTOBER, 1)
        endDate.set(2021, Calendar.NOVEMBER, 30)
        assertEquals(
            280,
            DateCalculator.calculateWorkingDays(
                startDate.timeInMillis,
                endDate.timeInMillis,
            )
        )
    }
}