package com.example.mangtas_exam

import java.util.*

data class Holiday(
    val month: Int = 0,
    val day: Int = 0,
    val type: HolidayType? = null,
    val dynamic: DynamicHoliday = DynamicHoliday()
)

data class DynamicHoliday(
    val dayOfWeek: Int = Calendar.MONDAY,
    val dayOfWeekInMonth: Int = 3,
    val month: Int = Calendar.NOVEMBER
)

enum class HolidayType {
    FIXED,
    WEEKEND,
    DYNAMIC
}